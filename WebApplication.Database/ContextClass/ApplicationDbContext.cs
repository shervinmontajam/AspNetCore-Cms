﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Domain.DatabaseModels;
using WebApplication.Domain.DatabaseModelsConfigs;
using WebApplication.Domain.Globals;

namespace WebApplication.Database.ContextClass
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        private readonly string _connectionString;


        public DbSet<ProductCategory> ProductCategoriesList { get; set; }
        public DbSet<Product> ProductsList { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IConfiguration configuration) : base(options)
        {
            _connectionString = configuration.GetConnectionString(GlobalStrings.ApplicationConnectionStringName);

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_connectionString, a => a.MigrationsAssembly("WebApplication.Database"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);


            builder.ApplyConfiguration(new ProductCategory_Config());
            builder.ApplyConfiguration(new Product_Config());


        }
    }
}
