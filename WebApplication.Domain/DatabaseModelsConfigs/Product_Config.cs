﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Domain.DatabaseModels;
using WebApplication.Domain.Globals;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Domain.DatabaseModelsConfigs
{
    public class Product_Config : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(GlobalStrings.ProductsName, GlobalStrings.ProductSchemaName);
        }
    }
}
