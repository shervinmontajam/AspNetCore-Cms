﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Domain.DatabaseModels;
using WebApplication.Domain.Globals;

namespace WebApplication.Domain.DatabaseModelsConfigs
{
    public class ProductCategory_Config : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            builder.ToTable(GlobalStrings.ProductCategoriesName, GlobalStrings.ProductSchemaName);
            builder.HasMany(child => child.Product_List).WithOne(parent => parent.ProductCategory_Navigation).IsRequired();
        }
    }
}
