﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Domain.DatabaseModels
{
    public class ProductCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }



        public virtual ICollection<Product> Product_List { get; set; }

    }
}
