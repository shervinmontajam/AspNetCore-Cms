﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Domain.Globals
{
    public class GlobalStrings
    {
        //--> App Configs
        public static readonly string ApplicationConnectionStringName = "DefaultConnection";


        //--> Databse Configs
        //--> Product Schema
        public static readonly string ProductSchemaName = "product";
        public static readonly string ProductCategoriesName = "ProductCategories";
        public static readonly string ProductsName = "Products";


    }
}
